package org.example;


import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * @author dingqianwen
 */
@Command(name = "Example Launcher", version = "1.0.0", mixinStandardHelpOptions = true)
public final class Launcher implements Runnable {

    /**
     * PicoCLI command line instance used for parsing
     */
    private final CommandLine cmdLine;

    /**
     * Main application entry point which executes the launcher and exits afterwards.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        final var launcher = new Launcher();
        System.exit(launcher.execute(args));
    }

    public Launcher() {
        this.cmdLine = new CommandLine(this);
    }

    @Override
    public void run() {
        final GpioController gpio = GpioFactory.getInstance();
        Application application = new Application(gpio);
        try {
            application.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gpio.shutdown();
    }

    /**
     * Uses PicoCLI to parse the given command line arguments and returns an appropriate POSIX exit code.
     *
     * @param args List of command line arguments to parse, usually provided by the main entrypoint of a Java application
     * @return Exit code after running the requested command
     */
    public int execute(String[] args) {
        return this.cmdLine.execute(args);
    }


}
