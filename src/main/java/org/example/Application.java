package org.example;


import com.pi4j.io.gpio.*;
import org.example.app.Display12832App;


/**
 * 〈Application〉
 *
 * @author 丁乾文
 * @date 2022/5/26 01:36
 * @since 1.0.0
 */
public class Application {


    private final GpioController gpio;

    public Application(GpioController gpio) {
        this.gpio = gpio;
    }


    public void execute() throws Exception {
        // 1、笑脸
        // new SmilingFaceApp(this.gpio).execute();
        // 2、贪吃蛇
        // new SnakeApp(this.gpio).execute();
        // 3、矩阵屏幕显示文字
        // new TextPrintApp(this.gpio).execute();
        // 4、oled 12832显示器
        new Display12832App(this.gpio).execute();
    }


    public GpioController getGpio() {
        return this.gpio;
    }

}
