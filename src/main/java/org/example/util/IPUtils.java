package org.example.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author 丁乾文
 * @date 2019/8/14
 * @since 1.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IPUtils {

    /**
     * 服务端的ip地址
     */
    public static final String SERVER_IP = getServerIp();

    /**
     * 获取服务端ip
     *
     * @return 服务端ip地址
     */
    private static String getServerIp() {
        String clientIp = "";
        //根据网卡取本机配置的IP,定义网络接口枚举类
        Enumeration<NetworkInterface> allNetInterfaces;
        try {
            //获得网络接口
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            //声明一个InetAddress类型ip地址
            InetAddress ip;
            //遍历所有的网络接口
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = allNetInterfaces.nextElement();
                //同样再定义网络地址枚举类
                Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    ip = addresses.nextElement();
                    //InetAddress类包括Inet4Address和Inet6Address
                    if ((ip instanceof Inet4Address)) {
                        if (!"127.0.0.1".equals(ip.getHostAddress())) {
                            clientIp = ip.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return clientIp;
    }

}
