package org.example.app;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import org.example.Application;
import org.example.components.MAX7219;

/**
 * 〈SmilingFaceApp〉
 *
 * @author 丁乾文
 * @date 2022/5/29 01:22
 * @since 1.0.0
 */
public class SmilingFaceApp extends Application {

    public static int[][] xl1 = {
            {0, 0, 1, 1, 1, 1, 0, 0},
            {0, 1, 0, 0, 0, 0, 1, 0},
            {1, 0, 1, 0, 0, 1, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 1, 0, 0, 1, 0, 1},
            {1, 0, 0, 1, 1, 0, 0, 1},
            {0, 1, 0, 0, 0, 0, 1, 0},
            {0, 0, 1, 1, 1, 1, 0, 0}
    };
    public static char[] xl2 = {0x3c, 0x42, 0xa5, 0x81, 0xa5, 0x99, 0x42, 0x3c};

    public SmilingFaceApp(GpioController gpio) {
        super(gpio);
    }

    @Override
    public void execute() throws Exception {
        // 初始化屏幕
        SpiDevice spiDevice = SpiFactory.getInstance(SpiChannel.CS0, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
        MAX7219 max7219 = new MAX7219(spiDevice);
        max7219.setBrightness(11);
        // 先清除屏幕
        max7219.clear();
        max7219.refresh();
        // 开机
        max7219.setEnabled(true);

        // 2进制方式
        // max7219.push(xl1);
        // 16进制方式
        max7219.push(xl2);

        Thread.sleep(10000);
        max7219.setEnabled(false);
    }

}
