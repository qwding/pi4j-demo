package org.example.app;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import org.example.Application;
import org.example.components.MAX7219;


/**
 * 〈TextPrintApp〉
 *
 * @author 丁乾文
 * @date 2022/5/29 01:51
 * @since 1.0.0
 */
public class TextPrintApp extends Application {

    public TextPrintApp(GpioController gpio) {
        super(gpio);
    }

    @Override
    public void execute() throws Exception {
        // 初始化屏幕
        SpiDevice spiDevice = SpiFactory.getInstance(SpiChannel.CS0, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
        MAX7219 max7219 = new MAX7219(spiDevice);
        max7219.setBrightness(12);
        // 先清除屏幕
        max7219.clear();
        max7219.refresh();
        // 开机
        max7219.setEnabled(true);

        // 中
        char[] text1 = {0x08, 0x7F, 0x49, 0x49, 0x7F, 0x08, 0x08, 0x08};
        // 国
        char[] text2 = {0xFE, 0xBA, 0x92, 0xBA, 0x92, 0x9A, 0xBA, 0xFE};
        // 笑脸
        char[] text3 = {0x3c, 0x42, 0xa5, 0x81, 0xa5, 0x99, 0x42, 0x3c};

        max7219.push(text3);


        Thread.sleep(10000);
        max7219.setEnabled(false);
    }

}
