package org.example.app;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.pi4j.io.gpio.GpioController;
import org.example.Application;
import org.example.components.OLEDDisplayDriver;
import org.example.util.IPUtils;
import oshi.SystemInfo;

import java.awt.*;
import java.awt.image.BufferedImage;


/**
 * 〈Dispaly12832〉
 *
 * @author 丁乾文
 * @date 2022/6/4 15:53
 * @since 1.0.0
 */
public class Display12832App extends Application {

    public Display12832App(GpioController gpio) {
        super(gpio);
    }

    @Override
    public void execute() throws Exception {
        OLEDDisplayDriver instance = OLEDDisplayDriver.getInstance();
        instance.initOLEDDisplay();
        while (true) {
            instance.display(this.getCover());
            ThreadUtil.sleep(1000);
        }
        //Thread.sleep(2000);
        //instance.shutdown();
    }


    public BufferedImage getCover() {
        SystemInfo info = new SystemInfo();
        BufferedImage image = new BufferedImage(128, 32, BufferedImage.TYPE_BYTE_BINARY);
        Graphics2D g = image.createGraphics();
        g.setFont(new Font("Default", Font.PLAIN, 10));
        g.setColor(new Color(0xffffff));
        g.drawString("TEMP：" + info.getHardware().getSensors().getCpuTemperature() + "C", 0, 10);
        g.drawString("DT：" + DateUtil.date(), 0, 21);
        g.drawString("IP：" + IPUtils.SERVER_IP, 0, 32);
        return image;
    }


}
