package org.example.app;


import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import org.example.Application;
import org.example.components.MAX7219;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 〈贪吃蛇〉
 *
 * @author 丁乾文
 * @date 2022/5/25 20:23
 * @since 1.0.0
 */
public class SnakeApp extends Application {

    private final GpioController gpio;

    private final MAX7219 max7219;

    public static int[][] xl = {
            {0, 0, 1, 1, 1, 1, 0, 0},
            {0, 1, 0, 0, 0, 0, 1, 0},
            {1, 0, 1, 0, 0, 1, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 1, 0, 0, 1, 0, 1},
            {1, 0, 0, 1, 1, 0, 0, 1},
            {0, 1, 0, 0, 0, 0, 1, 0},
            {0, 0, 1, 1, 1, 1, 0, 0}
    };

    public static int[][] sb = {
            //
            {1, 1, 0, 0, 0, 0, 1, 1},
            {1, 0, 1, 1, 1, 1, 0, 1},
            {0, 1, 0, 1, 1, 0, 1, 0},
            {0, 1, 1, 0, 0, 1, 1, 0},
            {0, 1, 1, 0, 0, 1, 1, 0},
            {0, 1, 0, 1, 1, 0, 1, 0},
            {1, 0, 1, 1, 1, 1, 0, 1},
            {1, 1, 0, 0, 0, 0, 1, 1}
    };

    private final Snake snake = new Snake(900, 1);

    private boolean suspend = false;

    public SnakeApp(GpioController gpio) throws IOException {
        super(gpio);
        this.gpio = gpio;
        // 初始化屏幕
        SpiDevice spiDevice = SpiFactory.getInstance(SpiChannel.CS0, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
        max7219 = new MAX7219(spiDevice);
        max7219.setBrightness(11);
        // 先清除屏幕
        max7219.clear();
        max7219.refresh();
        // 开机
        max7219.setEnabled(true);
    }

    @Override
    public void execute() throws InterruptedException {
        // 开机显示灯
        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, PinState.HIGH);
        pin.setShutdownOptions(true, PinState.LOW);
        pin.high();

        // 按键
        GpioPinDigitalInput[] inputs = {
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_29),
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_25),
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_28),
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_27),
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_26),
        };
        gpio.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                if (event.getState().getValue() != 1) {
                    // 按键抬起时，不触发
                    return;
                }
                if (Objects.equals(event.getPin().getPin(), RaspiPin.GPIO_25)) {
                    snake.upper();
                } else if (Objects.equals(event.getPin().getPin(), RaspiPin.GPIO_28)) {
                    snake.lower();
                } else if (Objects.equals(event.getPin().getPin(), RaspiPin.GPIO_27)) {
                    snake.left();
                } else if (Objects.equals(event.getPin().getPin(), RaspiPin.GPIO_26)) {
                    snake.right();
                } else if (Objects.equals(event.getPin().getPin(), RaspiPin.GPIO_29)) {
                    suspend = !suspend;
                }
            }
        }, inputs);
        while (snake.status == 0) {
            if (suspend) {
                Thread.sleep(20);
            } else {
                this.refreshDisplay();
                // 蛇移动速度
                Thread.sleep(snake.walkingSpeed);
                // 蛇不停的移动
                snake.move();
            }
        }
        if (snake.status == 1) {
            max7219.push(sb);
        } else if (snake.status == 2) {
            max7219.push(xl);
        }
        Thread.sleep(10000);
        // 清屏关机
        max7219.clear().refresh().setEnabled(false);
    }

    public void refreshDisplay() {
        for (int y = 0; y < snake.map.length; y++) {
            for (int x = 0; x < snake.map[y].length; x++) {
                if (snake.food.x == x && snake.food.y == y) {
                    max7219.setPixel(x, y, true);
                    continue;
                }
                if (snake.hitSelf(x, y)) {
                    max7219.setPixel(x, y, true);
                    continue;
                }
                max7219.setPixel(x, y, false);
            }
        }
        max7219.refresh();
    }

}


class Snake {

    /**
     * 初始化地图
     */
    public int[][] map = new int[8][8];

    /**
     * 当前食物位置
     */
    public Coordinate food;

    /**
     * x y 蛇的身体  模拟被点亮的灯坐标
     */
    public List<Coordinate> body = new LinkedList<>();

    /**
     * 爬行速度
     */
    public int walkingSpeed;

    /**
     * 当前行走方向
     */
    public volatile int direction = 1;

    /**
     * 0 正在爬行  1 蛇已经死亡  2 蛇完成挑战
     */
    public volatile int status = 0;

    public Snake(int walkingSpeed, int direction) {
        this.walkingSpeed = walkingSpeed;
        this.direction = direction;

        // 初始化身体
        body.add(new Coordinate(4, 4));
        body.add(new Coordinate(4, 5));
        body.add(new Coordinate(4, 6));
        // 初始化食物
        food = this.randomFood();
    }

    public synchronized void upper() {
        if (direction == 2) {
            // 不可回头
            return;
        }
        direction = 1;
    }

    public synchronized void lower() {
        if (direction == 1) {
            return;
        }
        direction = 2;
    }

    public synchronized void right() {
        if (direction == 3) {
            return;
        }
        direction = 4;
    }

    public synchronized void left() {
        if (direction == 4) {
            return;
        }
        direction = 3;
    }

    public synchronized void move() {
        // 获取头部坐标
        Coordinate st = this.body.get(0);
        int newY = st.y;
        int newX = st.x;
        switch (direction) {
            case 1:
                // 上
                newY = st.y - 1;
                break;
            case 2:
                // 下
                newY = st.y + 1;
                break;
            case 3:
                // 左
                newX = st.x - 1;
                break;
            case 4:
                // 右
                newX = st.x + 1;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + direction);
        }
        // 碰到右侧墙壁，头从左侧出来
        if (newX > 7) {
            newX = 0;
        }
        // 碰到左侧侧墙壁，头从右侧出来
        if (newX < 0) {
            newX = 7;
        }
        if (newY > 7) {
            newY = 0;
        }
        if (newY < 0) {
            newY = 7;
        }
        // 是否吃到了自己
        boolean hitSelf = this.hitSelf(newX, newY);
        if (hitSelf) {
            System.out.println("吃到自己，游戏结束");
            this.status = 1;
            return;
        }
        // 可以走
        this.body.add(0, new Coordinate(newX, newY));
        // 是否吃到了食物
        if (food.x == newX && food.y == newY) {
            // 吃到了食物,食物的位置重新随机
            food = this.randomFood();
        } else {
            this.body.remove(this.body.size() - 1);
        }
        if (this.body.size() >= 55) {
            System.out.println("你赢了");
            this.status = 2;
        }
    }

    /**
     * 坐标是否为自己身体某一部分
     *
     * @param x x
     * @param y y
     * @return true 是
     */
    public boolean hitSelf(int x, int y) {
        for (Coordinate s : body) {
            if (s.x == x && s.y == y) {
                return true;
            }
        }
        return false;
    }


    /**
     * 随机一个食物
     */
    public Coordinate randomFood() {
        List<Coordinate> foods = new ArrayList<>();
        for (int y = 0; y < map.length; y++) {
            for (int x = 0; x < map[y].length; x++) {
                // 排除掉自己，不会在自己身上出现食物
                if (this.hitSelf(x, y)) {
                    continue;
                }
                foods.add(new Coordinate(x, y));
            }
        }
        return foods.remove(ThreadLocalRandom.current().nextInt(0, foods.size()));
    }


    /**
     * debug
     */
    public void debugPrintMap() {
        System.out.println();
        System.out.println();
        for (int y = 0; y < map.length; y++) {
            for (int x = 0; x < map[y].length; x++) {
                // 食物坐标
                if (food.x == x && food.y == y) {
                    System.out.print("2  ");
                    continue;
                }
                // 蛇身体
                if (this.hitSelf(x, y)) {
                    System.out.print("1  ");
                    continue;
                }
                System.out.print("0  ");
            }
            System.out.println();
        }
    }


}


class Coordinate {
    int x;
    int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
