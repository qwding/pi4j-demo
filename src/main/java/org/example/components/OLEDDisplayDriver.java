package org.example.components;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.util.Arrays;

/**
 * 〈OLEDDisplayDriver〉
 *
 * @author 丁乾文
 * @date 2022/6/4 16:00
 * @since 1.0.0
 */
public class OLEDDisplayDriver {

    private static final int DEFAULT_I2C_BUS = I2CBus.BUS_1;
    private static final int DEFAULT_DISPLAY_ADDRESS = 0x3C;

    static final int DISPLAY_WIDTH = 128;
    static final int DISPLAY_HEIGHT = 64;
    private static final int MAX_INDEX = (DISPLAY_HEIGHT / 8) * DISPLAY_WIDTH;

    private static final byte SSD1306_SETCONTRAST = (byte) 0x81;
    private static final byte SSD1306_DISPLAYALLON_RESUME = (byte) 0xA4;
    private static final byte SSD1306_DISPLAYALLON = (byte) 0xA5;
    private static final byte SSD1306_NORMALDISPLAY = (byte) 0xA6;
    private static final byte SSD1306_INVERTDISPLAY = (byte) 0xA7;
    private static final byte SSD1306_DISPLAYOFF = (byte) 0xAE;
    private static final byte SSD1306_DISPLAYON = (byte) 0xAF;

    private static final byte SSD1306_SETDISPLAYOFFSET = (byte) 0xD3;
    private static final byte SSD1306_SETCOMPINS = (byte) 0xDA;

    private static final byte SSD1306_SETVCOMDETECT = (byte) 0xDB;

    private static final byte SSD1306_SETDISPLAYCLOCKDIV = (byte) 0xD5;
    private static final byte SSD1306_SETPRECHARGE = (byte) 0xD9;

    private static final byte SSD1306_SETMULTIPLEX = (byte) 0xA8;

    private static final byte SSD1306_SETLOWCOLUMN = (byte) 0x00;
    private static final byte SSD1306_SETHIGHCOLUMN = (byte) 0x10;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    private static final byte SSD1306_SETSTARTLINE = (byte) 0x40;

    private static final byte SSD1306_MEMORYMODE = (byte) 0x20;
    private static final byte SSD1306_COLUMNADDR = (byte) 0x21;
    private static final byte SSD1306_PAGEADDR = (byte) 0x22;

    private static final byte SSD1306_COMSCANINC = (byte) 0xC0;
    private static final byte SSD1306_COMSCANDEC = (byte) 0xC8;

    private static final byte SSD1306_SEGREMAP = (byte) 0xA0;

    private static final byte SSD1306_CHARGEPUMP = (byte) 0x8D;

    private static final byte SSD1306_EXTERNALVCC = (byte) 0x1;
    private static final byte SSD1306_SWITCHCAPVCC = (byte) 0x2;

    private static I2CBus bus;
    private static I2CDevice device;

    static {
        try {
            bus = I2CFactory.getInstance(DEFAULT_I2C_BUS);
            device = bus.getDevice(DEFAULT_DISPLAY_ADDRESS);
        } catch (I2CFactory.UnsupportedBusNumberException | IOException e) {
            e.printStackTrace();
        }
    }

    private OLEDDisplayDriver() {
    }

    private static final OLEDDisplayDriver OLED_DISPLAY_DRIVER = new OLEDDisplayDriver();

    public static OLEDDisplayDriver getInstance() {
        return OLED_DISPLAY_DRIVER;
    }

    private void writeCommand(byte command) throws IOException {
        device.write(0x00, command);
    }


    public void initOLEDDisplay() throws IOException {
        System.out.println("initOLEDDisplay i2c bus");
        writeCommand(SSD1306_DISPLAYOFF);                    // 0xAE
        writeCommand(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
        writeCommand((byte) 0x80);                           // the suggested ratio 0x80
        writeCommand(SSD1306_SETMULTIPLEX);                  // 0xA8
        writeCommand((byte) 0x3F);
        writeCommand(SSD1306_SETDISPLAYOFFSET);              // 0xD3
        writeCommand((byte) 0x0);                            // no offset
        writeCommand(SSD1306_SETSTARTLINE);   // line #0
        writeCommand(SSD1306_CHARGEPUMP);                    // 0x8D
        writeCommand((byte) 0x14);
        writeCommand(SSD1306_MEMORYMODE);                    // 0x20
        writeCommand((byte) 0x00);                           // 0x0 act like ks0108
        writeCommand((byte) (SSD1306_SEGREMAP | 0x1));
        writeCommand(SSD1306_COMSCANDEC);
        writeCommand(SSD1306_SETCOMPINS);                    // 0xDA
        writeCommand((byte) 0x12);
        writeCommand(SSD1306_SETCONTRAST);                   // 0x81
        writeCommand((byte) 0xCF);
        writeCommand(SSD1306_SETPRECHARGE);                  // 0xd9
        writeCommand((byte) 0xF1);
        writeCommand(SSD1306_SETVCOMDETECT);                 // 0xDB
        writeCommand((byte) 0x40);
        writeCommand(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
        writeCommand(SSD1306_NORMALDISPLAY);
        // turn on oled panel
        writeCommand(SSD1306_DISPLAYON);
    }

    /**
     * sends the current buffer to the display
     *
     * @throws IOException e
     */
    public synchronized void display(Image srcImg) throws IOException {
        writeCommand(SSD1306_COLUMNADDR);
        writeCommand((byte) 0);   // Column start address (0 = reset)
        writeCommand((byte) (DISPLAY_WIDTH - 1)); // Column end address (127 = reset)

        writeCommand(SSD1306_PAGEADDR);
        writeCommand((byte) 0); // Page start address (0 = reset)
        writeCommand((byte) 7); // Page end address

        BufferedImage bufferedImage = new BufferedImage(DISPLAY_WIDTH, DISPLAY_HEIGHT, BufferedImage.TYPE_BYTE_BINARY);
        Graphics graphics = bufferedImage.getGraphics();

        // 将原始位图按屏幕大小缩小后绘制到bufferedImage对象中
        graphics.drawImage(srcImg, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, null);

        byte[] pixels = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
        byte[] pixelsNew = new byte[DISPLAY_HEIGHT * DISPLAY_WIDTH / 8];
        Arrays.fill(pixelsNew, (byte) 0x00);
        int index = 0;
        int pixelIndex;
        for (int y = 0; y < DISPLAY_HEIGHT / 8; y++) {
            for (int x = 0; x < DISPLAY_WIDTH; x++) {
                for (int height = 7; height >= 0; height--) {
                    pixelsNew[index] = (byte) (pixelsNew[index] << 1);
                    pixelIndex = (y * 8 + height) * 16 + x / 8;
                    int value = (byte) ((pixels[pixelIndex] >> (7 - (x % 8))));
                    value &= 0x01;
                    if (value > 0) {
                        pixelsNew[index] |= 0x01;
                    } else {
                        pixelsNew[index] &= ~(0x01);
                    }
                }
                index++;
            }
        }
        for (int i = 0; i < ((DISPLAY_WIDTH * DISPLAY_HEIGHT / 8) / 16); i++) {
            // 每行写入16个字节
            device.write((byte) 0x40, pixelsNew, i * 16, 16);
        }
        writeCommand((byte) 0x2e);
    }

    public synchronized void shutdown() {
        try {
            // now we close the bus
            device.write(0x00, SSD1306_DISPLAYOFF);
            bus.close();
        } catch (IOException ex) {
            System.out.println("Closing i2c bus");
        }
    }

}
