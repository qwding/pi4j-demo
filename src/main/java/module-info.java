open module org.example {

    // SLF4J Modules
    requires org.slf4j;
    requires org.slf4j.simple;

    // PicoCLI Modules
    requires info.picocli;

    // AWT
    requires java.desktop;
    requires pi4j.core;

    requires com.github.oshi;

    requires hutool.all;
    requires lombok;
}
